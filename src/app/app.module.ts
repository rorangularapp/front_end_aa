import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { ApiService } from "./api.service";

import { AppComponent } from "./app.component";
import { PilotAddComponent } from "./pilot-add/pilot-add.component";
import { AircraftAddComponent } from "./aircraft-add/aircraft-add.component";
import { FlightAddComponent } from "./flight-add/flight-add.component";
import { FlightListComponent } from "./flight-list/flight-list.component";
import { AircraftListComponent } from "./aircraft-list/aircraft-list.component";
import { PilotListComponent } from "./pilot-list/pilot-list.component";

import { RouterModule } from "@angular/router";
import { FormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";
import { TablesViewComponent } from "./tables-view/tables-view.component";
@NgModule({
  declarations: [
    AppComponent,
    PilotAddComponent,
    AircraftAddComponent,
    FlightAddComponent,
    FlightListComponent,
    AircraftListComponent,
    PilotListComponent,
    TablesViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
      {
        path: "",
        component: TablesViewComponent
      },
      {
        path: "pilots",
        component: PilotListComponent
      },
      {
        path: "aircrafts",
        component: AircraftListComponent
      },
      {
        path: "flight_attributes",
        component: FlightListComponent
      },
      {
        path: "pilots/add/:id",
        component: PilotAddComponent
      },
      {
        path: "pilots/add",
        component: PilotAddComponent
      },
      {
        path: "aircrafts/add",
        component: AircraftAddComponent
      },
      {
        path: "aircrafts/add/:id",
        component: AircraftAddComponent
      },
      {
        path: "flight_attributes/add",
        component: FlightAddComponent
      },
      {
        path: "flight_attributes/add/:id",
        component: FlightAddComponent
      }
    ])
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule {}
