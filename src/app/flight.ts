export class Flight{
    public id:number;
    public flight_day:Date;
    public start_time:Date;
    public end_time:Date;
    public type_of_flight:string;
    public pilot_id:number;
    public aircraft_id:number;
    public pilot_name:string;
    public aircraft_name:string;

}