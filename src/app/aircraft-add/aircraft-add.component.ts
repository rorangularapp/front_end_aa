import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { ActivatedRoute } from '@angular/router';
import { Aircraft } from '../aircraft';

@Component({
  selector: 'app-aircraft-add',
  templateUrl: './aircraft-add.component.html',
  styleUrls: ['./aircraft-add.component.css']
})
export class AircraftAddComponent implements OnInit {
  public aircraft: Aircraft = new Aircraft();

  constructor(public apiService: ApiService, public acRouter: ActivatedRoute) { }

  ngOnInit() {
    this.acRouter.params.subscribe((data:any) =>{
      console.log(data);

      if(data && data.id){
        this.apiService.get('aircrafts/' + data.id).subscribe((data:Aircraft) =>{
          this.aircraft = data;
        });
      }else{
        this.aircraft = new Aircraft();
      }
    });
  }

  public onSubmit(){
    console.log("add aircraft: " + this.aircraft.brand + " " + this.aircraft.model);

    if(this.aircraft.id){
      this.apiService.update('aircrafts/' + this.aircraft.id, this.aircraft)
      .subscribe((r) => {
        console.log(r);
        alert("aricraft update");
      });
    }else{
      this.apiService.post("aircrafts", this.aircraft)
      .subscribe((r) => {
        this.aircraft = new Aircraft();
        alert("add new aircraft");
      });
    }
  }

}
