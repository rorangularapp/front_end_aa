import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../api.service";
import { Pilot } from "../pilot";
import { Aircraft } from "../aircraft";
import { Flight } from "../flight";

@Component({
  selector: "app-tables-view",
  templateUrl: "./tables-view.component.html",
  styleUrls: ["./tables-view.component.css"]
})
export class TablesViewComponent implements OnInit {
  public pilotColumns = ["Id", "Name", "Number of license"];
  public pilotRows: Array<Pilot>;

  public flightColumns = [
    "Id",
    "Flight Day",
    "Start Time",
    "End Time",
    "Fligth Type",
    "Pilot Name",
    "Aircraft Name"
  ];
  public flightRows: Array<Flight>;

  public aircraftColumns = ["Id", "Brand", "Model", "Plate", "Description"];
  public aircraftRows: Array<Aircraft>;

  public aemetTime: Object;

  constructor(public apiService: ApiService, public router: Router) {}

ngOnInit() {
    this.apiService.getAemet().subscribe(d => {
      this.getAemetTimeData(d);
    }); 
    this.getPilots();
  }
  getPilots(){
    this.apiService.get("pilots").subscribe((data: Pilot[]) => {
      this.pilotRows = data;
      this.getAircrafts();
    });
  }

  getAircrafts(){
    this.apiService.get("aircrafts").subscribe((data: Aircraft[]) => {
      this.aircraftRows = data;
      this.getFlights();
    });
  }

  getFlights(){
    this.apiService.get("flight_attributes").subscribe((data: Flight[]) => {
      this.flightRows = data;
      this.setNamePilot();  
    });
  }

  getAemetTimeData = data => {
    return this.apiService.getAemetTimeURL(data.datos).subscribe(d => {
      this.aemetTime = d;
    });
  };

 setNamePilot(){

    for(let flight = 0;flight < this.flightRows.length; flight++){
      for(let pilot = 0; pilot < this.pilotRows.length; pilot++){
        if(this.flightRows[flight].pilot_id === this.pilotRows[pilot].id){
          this.flightRows[flight].pilot_name = this.pilotRows[pilot].name +  " "
                                               this.pilotRows[pilot].lastName;
        }
      }
    }
    this.setAircraftName();
  }
  
  setAircraftName(){
    for(let flight = 0;flight < this.flightRows.length; flight++){
      for(let aircraft = 0; aircraft < this.aircraftRows.length; aircraft++){
        if(this.flightRows[flight].aircraft_id === this.aircraftRows[aircraft].id){
          this.flightRows[flight].aircraft_name = this.aircraftRows[aircraft].brand +  " "
                                               this.aircraftRows[aircraft].model;
        }
      }
    }
  }
}
