export class Pilot{
    public id:number;
    public name:string;
    public lastName:string;
    public numberOfLicence:string;
}