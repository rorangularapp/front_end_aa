import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class ApiService {
  API_URL: string = "http://localhost:3000/";
  dataTime;

  constructor(public http: HttpClient) {}

  //read method
  public get(path: string) {
    let endpoint = this.API_URL + path;
    return this.http.get(endpoint);
  }

  //create method
  public post(path: string, body: any) {
    let endpoint = this.API_URL + path;
    return this.http.post(endpoint, body);
  }

  //delete method
  public delete(path: string) {
    let endpoint = this.API_URL + path;
    return this.http.delete(endpoint);
  }

  //update method
  public update(path: string, body: any) {
    let endpoint = this.API_URL + path;
    return this.http.put(endpoint, body);
  }

  getAemet = () => {
    let aemetUrl =
      "https://opendata.aemet.es/opendata/api/valores/climatologicos/inventarioestaciones/todasestaciones/?api_key=eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJhbGZvbnNvLmdhcmNpYUB3b3JsZHNhdG5ldC5jb20iLCJqdGkiOiI5NDI5ZmY0My04MTliLTQ2ZDYtYWFmNC1mZDIzMmE0ZWI0MDYiLCJpc3MiOiJBRU1FVCIsImlhdCI6MTUxOTgxNDk0NCwidXNlcklkIjoiOTQyOWZmNDMtODE5Yi00NmQ2LWFhZjQtZmQyMzJhNGViNDA2Iiwicm9sZSI6IiJ9.ul5VvIq0nwIOEhMiqR4bIRldpYf1Opk0MIPfn0Sb6-4";
    return this.http.get(aemetUrl);
  };

  getAemetTimeURL = data => {
    let timeUrl = data;
    return this.http.get(timeUrl);
  };
}
