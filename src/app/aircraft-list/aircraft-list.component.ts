import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {ApiService} from '../api.service';
import { Aircraft } from '../aircraft';

@Component({
  selector: 'app-aircraft-list',
  templateUrl: './aircraft-list.component.html',
  styleUrls: ['./aircraft-list.component.css']
})
export class AircraftListComponent implements OnInit {
  public columns = ['Id', 'Brand', 'Model', 'Plate', 'Description'];
  public rows: Array<Aircraft>;
  constructor(public apiService: ApiService, public router: Router) { }

  ngOnInit() {
    this.apiService.get("aircrafts").subscribe((data: Aircraft[]) =>{
      console.log(data);

      this.rows = data;
    });
  }

  public delete(id:string){
    console.log("delete: " + id);
    let path = 'aircrafts/' + id;

    this.apiService.delete(path).subscribe((r) => {
      this.rows = this.rows.filter((a,i) =>{
        if(Number(id) == a.id){
          return false;
        }
        return true;
      }, this.rows);
    });
  }

  public update(id:string){
    console.log("update: " + id);

    this.router.navigateByUrl('aircrafts/add/' + id);
  }

}
