import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../api.service';
import { Pilot } from '../pilot';

@Component({
  selector: 'app-pilot-list',
  templateUrl: './pilot-list.component.html',
  styleUrls: ['./pilot-list.component.css']
})
export class PilotListComponent implements OnInit {
  public columns = ['Id', 'Name'];
  public rows: Array<Pilot>;
  constructor(public apiService: ApiService, public router:Router) { }

  ngOnInit() {
    this.apiService.get("pilots").subscribe((data : Pilot[]) => {
      console.log(data);
      this.rows = data;
      
    })
  }
  public delete(id:string){
    console.log("delete: " + id);
    let path = 'pilots/' + id;
    this.apiService.delete(path).subscribe((r) =>{
      this.rows = this.rows.filter((p,i) =>{
        if(Number(id) == p.id){
          return false;
        }
        return true;
      }, this.rows)
    })
  }

  public update(id:string){
    console.log("update: " +  id);
    this.router.navigateByUrl('pilots/add/' + id);
  }
}
