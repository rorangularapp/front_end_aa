export class Aircraft{
    public id:number;
    public brand:string;
    public model:string;
    public plate:string;
    public description:string;
}