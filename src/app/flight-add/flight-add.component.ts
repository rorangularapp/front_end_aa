import { Component, OnInit } from "@angular/core";
import { ApiService } from "../api.service";
import { ActivatedRoute } from "@angular/router";
import { Flight } from "../flight";
import { Pilot } from "../pilot";
import { Aircraft } from "../aircraft";

@Component({
  selector: "app-flight-add",
  templateUrl: "./flight-add.component.html",
  styleUrls: ["./flight-add.component.css"]
})
export class FlightAddComponent implements OnInit {
  public flight: Flight = new Flight();
  pilots: Array<Pilot>;
  aircrafts: Array<Aircraft>;

  constructor(public apiService: ApiService, public acRoute: ActivatedRoute) {}

  ngOnInit() {
    this.acRoute.params.subscribe((data: any) => {
      console.log(data);
      if (data && data.id) {
        this.apiService
          .get("flight_attributes/" + data.id)
          .subscribe((data: Flight) => {
            this.flight = data;
          });
      } else {
        this.flight = new Flight();
      }
    });

    this.apiService.get("pilots").subscribe((data: Pilot[]) => {
      this.pilots = data;
    });

    this.apiService.get("aircrafts").subscribe((data: Aircraft[]) => {
      this.aircrafts = data;
    });
  }

  public onSubmit() {
    if (this.flight.id) {
      this.apiService
        .update("flight_attributes/" + this.flight.id, this.flight)
        .subscribe(r => {
          console.log(r);
          alert("fligth update");
        });
    } else {
      console.log(this.flight);
      this.apiService.post("flight_attributes", this.flight).subscribe(r => {
        console.log(r);
        this.flight = new Flight();
        alert("add new flight");
      });
    }
  }


}
