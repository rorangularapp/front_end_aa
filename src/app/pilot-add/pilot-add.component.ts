import { Component, OnInit } from "@angular/core";
import { Pilot } from "../pilot";
import { ApiService } from "../api.service";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-pilot-add",
  templateUrl: "./pilot-add.component.html",
  styleUrls: ["./pilot-add.component.css"]
})
export class PilotAddComponent implements OnInit {
  public pilot: Pilot = new Pilot();
  constructor(public apiService: ApiService, public acRoute: ActivatedRoute) {}

  ngOnInit() {
    this.acRoute.params.subscribe((data: any) => {
      console.log(data);
      if (data && data.id) {
        this.apiService.get("pilots/" + data.id).subscribe((data: Pilot) => {
          this.pilot = data;
        });
      } else {
        this.pilot = new Pilot();
      }
    });
  }

  public onSubmit() {
    console.log("Adding pilot: " + this.pilot.name);
    if (this.pilot.id) {
      this.apiService
        .update("pilots/" + this.pilot.id, this.pilot)
        .subscribe(r => {
          console.log(r);
          alert("Pilot update");
        });
    } else {
      this.apiService.post("pilots", this.pilot).subscribe(r => {
        console.log(r);
        this.pilot = new Pilot();
        alert("Add Pilot");
      });
    }
  }
}
