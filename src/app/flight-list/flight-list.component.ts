import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { ApiService } from "../api.service";
import { Flight } from "../flight";
import { Pilot } from "../pilot";
import { Aircraft } from "../aircraft";

@Component({
  selector: "app-flight-list",
  templateUrl: "./flight-list.component.html",
  styleUrls: ["./flight-list.component.css"]
})
export class FlightListComponent implements OnInit {
  public columns = [
    "Id",
    "Flight Day",
    "Start Time",
    "End Time",
    "Fligth Type",
    "Pilot Name",
    "Aircraft Name"
  ];
  public rows: Array<Flight>;
  public fligth: Flight = new Flight();
  public pilotRows: Array<Pilot>;
  public aircraftRows: Array<Aircraft>;

  constructor(public router: Router, public apiService: ApiService) {}

  ngOnInit() {
    this.apiService.get("flight_attributes").subscribe((data: Flight[]) => {
      this.rows = data;
      this.getPilots();
    });

  }
  getPilots(){
    this.apiService.get("pilots").subscribe((data: Pilot[]) => {
      this.pilotRows = data;
      this.getAircrafts();
    });
  }

  getAircrafts(){
    this.apiService.get("aircrafts").subscribe((data: Aircraft[]) => {
      this.aircraftRows = data;
      this.setNamePilot();
    });
  }

 setNamePilot(){

    for(let flight = 0;flight < this.rows.length; flight++){
      for(let pilot = 0; pilot < this.pilotRows.length; pilot++){
        if(this.rows[flight].pilot_id === this.pilotRows[pilot].id){
          this.rows[flight].pilot_name = this.pilotRows[pilot].name +  " "
                                               this.pilotRows[pilot].lastName;
        }
      }
    }
    this.setAircraftName();
  }
  
  setAircraftName(){
    for(let flight = 0;flight < this.rows.length; flight++){
      for(let aircraft = 0; aircraft < this.aircraftRows.length; aircraft++){
        if(this.rows[flight].aircraft_id === this.aircraftRows[aircraft].id){
          this.rows[flight].aircraft_name = this.aircraftRows[aircraft].brand +  " "
                                               this.aircraftRows[aircraft].model;
        }
      }
    }
  }
  
  public delete(id: string) {
    let path = "flight_attributes/" + id;
    this.apiService.delete(path).subscribe(r => {
      this.rows = this.rows.filter((f, i) => {
        if (Number(id) == f.id) {
          return false;
        }
        return true;
      }, this.rows);
    });
  }

  public update(id: string) {
    this.router.navigateByUrl("flight_attributes/add/" + id);
  }
}
